<?php



namespace App\Http\Controllers;



use App\Licence;

use App\Workspace;

use Illuminate\Http\Request;



class UserController extends Controller

{

    public function getLicenceKey( $key )

    {

        return Licence::where('key',$key)->first();

    }



    public function getWorkspaceForLicence($key)

    {

        $keyID = Licence::where('key',$key)->first()->ID;

        return Workspace::where('key_id',$keyID)->get();

    }

    public function getLicenceKeys()
    {
        return Licence::first();
    }

    public function addWorkspace()

    {

        $workspace = new Workspace();

        $workspace->nazwa = "Zuzanna";

        $workspace->key_id = 1;

        $workspace->save();

        return true;

    }



}
